/*
 * Marcus Tuke
 * 27/04/2014
 */

import net.sf.picard.sam.FixMateInformation
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.util.QScriptUtils
import org.broadinstitute.sting.queue.extensions.picard._
import org.broadinstitute.sting.queue.function.ListWriterFunction
import collection.JavaConversions._
import net.sf.samtools.SAMFileReader
import net.sf.samtools.SAMFileHeader.SortOrder
import org.broadinstitute.sting.commandline.Hidden

import org.broadinstitute.sting.gatk.walkers.indels.IndelRealigner.ConsensusDeterminationModel

class BAMProcessingPipeline extends QScript {
  qscript =>

  /****************************************************************************
  * Required Parameters
  ****************************************************************************/

  @Input(doc="input list of fastq files. Prefixes only: i.e. /Sample_WE_323/raw_illumina_reads/WE_323_GCCAAT_L002 only once representing both fastq files", fullName="input", shortName="i", required=true)
  var input: File = _

  @Input(doc="Reference fasta file", fullName="reference", shortName="R", required=true)
  var reference: File = _

  @Input(doc="dbsnp ROD to use (must be in VCF format)", fullName="dbsnp", shortName="D", required=true)
  var dbSNP: Seq[File] = Seq()

  @Input(doc="ID for use in BWA", fullName="bwa_id", shortName="ID", required=true)
  var bwa_id: Seq[File] = Seq()

  /****************************************************************************
  * Optional Parameters
  ****************************************************************************/

  @Input(doc="extra VCF file to use as reference indels for Indel Realignment", fullName="extra_indels", shortName="indels", required=false)
  var indels: Seq[File] = Seq()

  @Input(doc="The path to the BWA binary", fullName="path_to_bwa", shortName="bwa", required=false)
  var bwaPath: File = _

  @Input(doc="the project name determines the final output (BAM file) base name. Example NA12878 yields NA12878.processed.bam", fullName="project", shortName="p", required=false)
  var projectName: String = "project"

  @Input(doc="Output path for the processed BAM files - default is current directory", fullName="output_directory", shortName="outputDir", required=false)
  var outputDir: String = ""

  @Input(doc="the -L interval string to be used by GATK - output bams at interval only", fullName="gatk_interval_string", shortName="L", required=false)
  var intervalString: String = ""

  @Input(doc="an intervals file to be used by GATK - output bams at intervals only", fullName="gatk_interval_file", shortName="intervals", required=false)
  var intervals: File = _

  @Input(doc="Cleaning model: KNOWNS_ONLY, USE_READS or USE_SW - use reads is default with -indels flag set for known data", fullName="clean_model", shortName="cm", required=false)
  var cleaningModel: String = "USE_READS"

  @Input(doc="Number of threads BWA should use - default is 4", fullName="bwa_threads", shortName="bt", required=false)
  var bwaThreads: Int = 4

  /****************************************************************************
  * Hidden Parameters (can set these flags, but they won't come out in the help info)
  ****************************************************************************/
  @Hidden
  @Input(doc="How many ways to scatter/gather - splits by equally sized number of reference contigs as default (84 with GRCb37)", fullName="scatter_gather", shortName="sg", required=false)
  var nContigs: Int = -1

  /****************************************************************************
  * Global Variables
  ****************************************************************************/

  val queueLogDir: String = ".qlog/"  // Gracefully hide Queue's output

  var cleanModelEnum: ConsensusDeterminationModel = ConsensusDeterminationModel.USE_READS
  var readFilter: Seq[String] = Seq("BadCigar")

  // Assign Indel realignment model from whatever parameter was set by user

  def getIndelCleaningModel: ConsensusDeterminationModel = {
    if (cleaningModel == "KNOWNS_ONLY")
      ConsensusDeterminationModel.KNOWNS_ONLY // Knowns only
    else if (cleaningModel == "USE_SW")
      ConsensusDeterminationModel.USE_SW // Smith waterman (slow)
    else
      ConsensusDeterminationModel.USE_READS // Default recommended
  }

  /****************************************************************************
  * Main script
  ****************************************************************************/

  def script() {
    // final output list of processed bam files
    
    var cohortList: Seq[File] = Seq()
    
    // sets the model for the Indel Realigner
    
    cleanModelEnum = getIndelCleaningModel

    // keep a record of the number of contigs in the first bam file in the list
    
    val fqs = QScriptUtils.createSeqFromFile(input)
    if (nContigs < 0)
     nContigs = 84

    val alignedBAMs = performAlignment(fqs)

    // if this is a 'knowns only' indel realignment run, do it only once for all samples
    val globalIntervals = new File(outputDir + projectName + ".intervals")
    if (cleaningModel == ConsensusDeterminationModel.KNOWNS_ONLY)
      add(target(null, globalIntervals))

    // put each sample through the pipeline
    for ((sample, bamList) <- sampleBAMFiles) {

      // BAM files generated by the pipeline

      var file = sample.split("/").last
      val bam        = new File(qscript.projectName + "." + sample + ".bam")
      val dedupedBam = swapExt(bam, ".bam", ".dedup.bam")
      val cleanedBam = swapExt(bam, ".bam", ".dedup.clean.bam")

      // Accessory files
      val targetIntervals = if (cleaningModel == ConsensusDeterminationModel.KNOWNS_ONLY) {globalIntervals} else {swapExt(bam, ".bam", ".intervals")}
      val metricsFile     = swapExt(bam, ".bam", ".metrics")

      if (cleaningModel != ConsensusDeterminationModel.KNOWNS_ONLY)
        add(dedup(bamList, dedupedBam, metricsFile),
          clean(dedupedBam, targetIntervals, cleanedBam))
      cohortList :+= cleanedBam
    }

    // output a BAM list with all the processed per sample files
    val cohortFile = new File(qscript.outputDir + qscript.projectName + ".cohort.list")
    add(writeList(cohortList, cohortFile))
  }

  /****************************************************************************
  * Helper classes
  ****************************************************************************/

  // BWA mem align and sort fastq files into list of bams

  def performAlignment(fqs: Seq[String]): Seq[File] = {
    var alignedBams: Seq[File] = Seq()
    for (fq <- fqs) {
      val alignedSamFile = fqs + ".sam"
      val alignedSamFile = fqs + ".bam"
      add(bwa_mem(revertedBAM, saiFile1), sortSam(realignedSamFile, realignedBamFile, SortOrder.coordinate))
      realignedBams :+= realignedBamFile
    }
    realignedBams
  }


  /****************************************************************************
  * GATK Walkers
  ****************************************************************************/

  // General arguments to non-GATK tools
  trait ExternalCommonArgs extends CommandLineFunction {
    this.memoryLimit = 4
    this.isIntermediate = true
  }

  // General arguments to GATK walkers
  trait CommandLineGATKArgs extends CommandLineGATK with ExternalCommonArgs {
    this.reference_sequence = qscript.reference
    this.read_filter = readFilter
  }

  trait SAMargs extends PicardBamFunction with ExternalCommonArgs {
    this.maxRecordsInMemory = 100000000
    this.maxReadsForRealignment = 600000
  }
  
  // Realign reads around indels
  case class clean (inBam: File, tIntervals: File, outBam: File) extends IndelRealigner with CommandLineGATKArgs {
    this.input_file :+= inBam
    this.targetIntervals = tIntervals
    this.out = outBam
    this.known ++= qscript.dbSNP
    if (qscript.indels != null)
      this.known ++= qscript.indels
    this.consensusDeterminationModel = cleanModelEnum
    this.compress = 0
    this.noPGTag = qscript.testMode;
    this.scatterCount = nContigs
    this.analysisName = queueLogDir + outBam + ".clean"
    this.jobName = queueLogDir + outBam + ".clean"
  }

  /****************************************************************************
   * non-GATK programs
   ****************************************************************************/


  // Class to fix mate information with Picard
  case class fmi (inBams: Seq[File], outBam: File, metricsFile: File) extends FixMateInformation with ExternalCommonArgs {
    this.input = inBams
    this.output = outBam
    this.metrics = metricsFile
    this.memoryLimit = 4 
    //this.max_open_temp_files = 1024
    this.analysisName = queueLogDir + outBam + ".dedup"
    this.jobName = queueLogDir + outBam + ".dedup"
  }

  // Class to Mark Ducplicates wirh Picard
  case class dedup (inBams: Seq[File], outBam: File, metricsFile: File) extends MarkDuplicates with ExternalCommonArgs {
    this.input = inBams
    this.output = outBam
    this.metrics = metricsFile
    this.memoryLimit = 4 
    //this.max_open_temp_files = 1024
    this.analysisName = queueLogDir + outBam + ".dedup"
    this.jobName = queueLogDir + outBam + ".dedup"
  }
  //class to sort a SAM and output BAM
  case class sortSam (inSam: File, outBam: File, sortOrderP: SortOrder) extends SortSam with ExternalCommonArgs {
    this.input :+= inSam
    this.output = outBam
    this.sortOrder = sortOrderP
    this.analysisName = queueLogDir + outBam + ".sortSam"
    this.jobName = queueLogDir + outBam + ".sortSam"
  }
  // Do BWA MEM alignment required a prefix path for a fastq pair
  case class bwa_mem (inFq: String, outSam: File, id: String) extends CommandLineFunction with ExternalCommonArgs {
    @Input(doc="fastq prefix to be aligned") var fq = inFq
    @Output(doc="output file") var sam = outSam
    var file = inFq.split("/").last
    var readGroup = "@RG\tID:" + id +"\tPL:ILLUMINA\tSM:" + sm +"\tLB:" + id + "_" + sm
    def commandLine = bwaPath + " mem -t " + bwaThreads + " -m -R '" + readGroup + "' " + qscript.reference + " " + fq + "_R1_001.fastq " + fq + "_R2_001.fastq > " + sam
    this.analysisName = queueLogDir + outSam, + ".bwa_mem"
    this.jobName = queueLogDir + outSam + ".bwa_mem"
  }
  // Write an output list after processing
  case class writeList(inBams: Seq[File], outBamList: File) extends ListWriterFunction {
    this.inputFiles = inBams
    this.listFile = outBamList
    this.analysisName = queueLogDir + outBamList + ".bamList"
    this.jobName = queueLogDir + outBamList + ".bamList"
  }
}
