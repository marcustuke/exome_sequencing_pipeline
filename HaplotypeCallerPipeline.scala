/*
 * Marcus Tuke
 * 17/04/2014
 */

import org.broadinstitute.sting.queue.QScript
import org.broadinstitute.sting.queue.extensions.gatk._
import org.broadinstitute.sting.queue.util.QScriptUtils

class HaplotypeCallerPipeline extends QScript {
  qscript =>

  // Required arguments:

  @Input(doc="Reference FASTA (REQUIRED).", shortName="R")
  var referenceFile: File = _

  @Input(doc="Input BAM file path list (REQUIRED).", shortName="I")
  var bamFile: File = _

  @Input(doc="An optional file with a list of intervals to proccess.", shortName="L", required=false)
  var intervals: File = _

  // Optional arguments:
  
  @Input(doc="dbSNP file.", shortName="D", required=false)
  var dbSNP: File = _

  @Argument(doc="Experimental reference confidence.", shortName="erc", required=false)
  var erc = org.broadinstitute.sting.gatk.walkers.haplotypecaller.HaplotypeCaller.ReferenceConfidenceMode.GVCF

  @Argument(doc="Variant Index Type.", shortName="vit", required=false)
  var vit = org.broadinstitute.sting.utils.variant.GATKVCFIndexType.LINEAR

  @Argument(doc="Variant Index Parameter.", shortName="vip", required=false)
  var vip: Int = 0

  @Argument(doc="Max Alt Alleles.", shortName="alt", required=false)
  var alt: Int = 0

  @Argument(doc="Stand Call Conf.", shortName="call", required=false)
  var call: Double = 0.0

  @Argument(doc="Stand Emit Conf.", shortName="emit", required=false)
  var emit: Double = 0.0

  //@Argument(doc="Log file.", shortName="lg", required=false)
  //var log: File = _

  @Argument(doc="Depth of Coverage.", shortName="dcov", required=false)
  var dcov: Int = 0

  // Nil is an empty List & null (_) is a non-existent List.

  // for each path
    trait CommonArgs extends CommandLineGATK {
      this.reference_sequence = qscript.referenceFile
      this.downsample_to_coverage = qscript.dcov
      this.memoryLimit = 4
      this.intervals = List(qscript.intervals)
    }
    // Run the pipeline
    def script() {
      val bams = QScriptUtils.createSeqFromFile(bamFile)
      for (bam <- bams) {
        val hc = new HaplotypeCaller with CommonArgs

        /* The below variable (scatterCount) sets the number of chunks that each sample is split in to. ~3000 ensures that each chunk runs for several minutes */
        hc.scatterCount = 5
        hc.dbsnp = qscript.dbSNP
        hc.log_to_file = swapExt(bam, "bam", "HC_3.1-1.log")
        hc.input_file :+= bam
        hc.emitRefConfidence = qscript.erc
        hc.variant_index_type = qscript.vit
        hc.variant_index_parameter = qscript.vip
        hc.max_alternate_alleles = qscript.alt
        hc.out = swapExt(bam, "bam", "HC_3.1-1.vcf")
        hc.standard_min_confidence_threshold_for_calling = qscript.call
        hc.standard_min_confidence_threshold_for_emitting = qscript.emit
        add(hc)
      }
   }
}
