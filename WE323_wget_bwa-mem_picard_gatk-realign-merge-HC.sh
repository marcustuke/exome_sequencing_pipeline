#wget ftp://Project_2102:mgvWuyZ2B4TmY@zeus-galaxy.ex.ac.uk/Sample_WE_323-l1/raw_illumina_reads/WE_323-l1_GCCAAT_L003_R1_001.fastq 
#wget ftp://Project_2102:mgvWuyZ2B4TmY@zeus-galaxy.ex.ac.uk/Sample_WE_323-l1/raw_illumina_reads/WE_323-l1_GCCAAT_L003_R2_001.fastq 
#(/usr/share/bwa/bwa-0.7.9a/bwa mem -t 4 -M -R '@RG\tID:C4H81ACXX\tPL:ILLUMINA\tSM:WE323\tLB:WE323_C4H81ACXX' /mnt/Data1/resources/human_g1k_v37.fasta WE_323-l1_GCCAAT_L003_R1_001.fastq WE_323-l1_GCCAAT_L003_R2_001.fastq > WE323.sam) >& logs/WE323.bwa-mem.log 
#java -Xmx8g -jar /usr/share/picard/picard-tools-1.114/SamFormatConverter.jar I=WE323.sam O=WE323.bam VALIDATION_STRINGENCY=SILENT TMP_DIR=/mnt/Data3/tmp 
#java -Xmx8g -jar /usr/share/picard/picard-tools-1.114/FixMateInformation.jar I=WE323.bam O=WE323.fixmate.bam VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true SORT_ORDER=coordinate TMP_DIR=/mnt/Data3/tmp 
#java -Xmx8g -jar /usr/share/picard/picard-tools-1.114/MarkDuplicates.jar I=WE323.fixmate.bam O=WE323.fixmate.nodup.bam METRICS_FILE=WE323.duplicates REMOVE_DUPLICATES=true VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true TMP_DIR=/mnt/Data3/tmp 
#java -Djava.io.tmpdir=/mnt/Data3/tmp -Xmx8g -jar /usr/share/gatk/GenomeAnalysisTK-3.1-1/GenomeAnalysisTK.jar -T RealignerTargetCreator -I WE323.fixmate.nodup.bam -o WE323.intervals -log logs/RealignerTargetCreator.WE323.log -known /mnt/Data1/resources/Mills_and_1000G_gold_standard.indels.b37.vcf -R /mnt/Data1/resources/human_g1k_v37.fasta 
#java -Djava.io.tmpdir=/mnt/Data3/tmp -Xmx8g -jar /usr/share/gatk/GenomeAnalysisTK-3.1-1/GenomeAnalysisTK.jar -T IndelRealigner -I WE323.fixmate.nodup.bam -o WE323.realigned.bam -targetIntervals WE323.intervals  -log logs/IndelRealigner.WE323.log -known /mnt/Data1/resources/Mills_and_1000G_gold_standard.indels.b37.vcf -R /mnt/Data1/resources/human_g1k_v37.fasta -compress 0 --maxReadsInMemory 100000000 --maxReadsForRealignment 600000 
#java -Xmx8g -jar /usr/share/picard/picard-tools-1.114/SortSam.jar I=WE323.realigned.bam O=WE323.realigned.sorted.bam SO=coordinate CREATE_INDEX=true TMP_DIR=/mnt/Data3/tmp 
#java -Djava.io.tmpdir=/mnt/Data3/tmp -Xmx8g -jar /usr/share/gatk/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T HaplotypeCaller -I WE323.realigned.sorted.bam -o WE323.HC_3.2-2.gVCF --emitRefConfidence GVCF --variant_index_type LINEAR --variant_index_parameter 128000 -log logs/HC_3.2-2.gVCF.WE323.log -L /mnt/Data2/exome_sequencing/SureSelect_All_Exon_V5_plusminus100bp.interval_list -D /mnt/Data1/resources/dbsnp_138.b37.vcf -R /mnt/Data1/resources/human_g1k_v37.fasta -stand_call_conf 50.0 -stand_emit_conf 10.0 -dcov 1000 


wget ftp://Project_2107:gjxXYp35SIBJ8@zeus-galaxy.ex.ac.uk/Sample_WE_323/raw_illumina_reads/WE_323_GCCAAT_L002_R1_001.fastq & 
wget ftp://Project_2107:gjxXYp35SIBJ8@zeus-galaxy.ex.ac.uk/Sample_WE_323/raw_illumina_reads/WE_323_GCCAAT_L002_R2_001.fastq & 
wait
(/usr/share/bwa/bwa-0.7.9a/bwa mem -t 4 -M -R '@RG\tID:HA3R4ADXX\tPL:ILLUMINA\tSM:WE323\tLB:WE323_HA3R4ADXX' /mnt/Data1/resources/human_g1k_v37.fasta WE_323_GCCAAT_L002_R1_001.fastq WE_323_GCCAAT_L002_R2_001.fastq > WE323.HA3R4ADXX.sam) >& logs/WE323.HA3R4ADXX.bwa-mem.log 
java -Xmx6g -jar /usr/share/picard/picard-tools-1.114/SamFormatConverter.jar I=WE323.HA3R4ADXX.sam O=WE323.HA3R4ADXX.bam VALIDATION_STRINGENCY=SILENT TMP_DIR=/mnt/Data3/tmp 
java -Xmx6g -jar /usr/share/picard/picard-tools-1.114/FixMateInformation.jar I=WE323.HA3R4ADXX.bam O=WE323.HA3R4ADXX.fixmate.bam VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true SORT_ORDER=coordinate TMP_DIR=/mnt/Data3/tmp 
java -Xmx4g -jar /usr/share/picard/picard-tools-1.114/MarkDuplicates.jar I=WE323.HA3R4ADXX.fixmate.bam O=WE323.HA3R4ADXX.fixmate.nodup.bam METRICS_FILE=WE323.HA3R4ADXX.duplicates REMOVE_DUPLICATES=true VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true TMP_DIR=/mnt/Data3/tmp 
java -Djava.io.tmpdir=/mnt/Data3/tmp -Xmx6g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T IndelRealigner -I WE323.HA3R4ADXX.fixmate.nodup.bam -o WE323.HA3R4ADXX.realigned.bam -targetIntervals WE323.intervals  -log logs/IndelRealigner.WE323.HA3R4ADXX.log -known /mnt/Data1/resources/Mills_and_1000G_gold_standard.indels.b37.vcf -R /mnt/Data1/resources/human_g1k_v37.fasta -compress 0 --maxReadsInMemory 100000000 --maxReadsForRealignment 600000 
java -Xmx6g -jar /usr/share/picard/picard-tools-1.114/SortSam.jar I=WE323.HA3R4ADXX.realigned.bam O=WE323.HA3R4ADXX.realigned.sorted.bam SO=coordinate CREATE_INDEX=true TMP_DIR=/mnt/Data3/tmp  

java -Djava.io.tmpdir=/mnt/Data3/tmp -jar /usr/share/picard/picard-tools-1.114/MergeSamFiles.jar I=WE323.realigned.sorted.bam I=WE323.HA3R4ADXX.realigned.sorted.bam O=WE323.realigned.sorted.merge2runs.bam CREATE_INDEX=true  


java -Djava.io.tmpdir=/mnt/Data3/tmp -Xmx8g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T HaplotypeCaller -I WE323.realigned.sorted.merge2runs.bam -o WE323.merge.HC-3.3-0.gVCF --emitRefConfidence GVCF --variant_index_type LINEAR --variant_index_parameter 128000 -log logs/HC-3.3-0.gVCF.WE323.merge.log -L /mnt/Data2/exome_sequencing/SureSelect_All_Exon_V5_plusminus100bp.interval_list -D /mnt/Data1/resources/dbsnp_141.b37.vcf -R /mnt/Data1/resources/human_g1k_v37.fasta -stand_call_conf 50.0 -stand_emit_conf 10.0 -dcov 1000  

java -Xmx6g -jar /usr/share/picard/picard-tools-1.114/CalculateHsMetrics.jar I=WE323.realigned.sorted.merge2runs.bam O=metrics/WE323.merge2runs.ExonV5_HS_metics BAIT_INTERVALS=../SureSelect_All_Exon_V5_S04380110_Regions.interval_list TARGET_INTERVALS=../SureSelect_All_Exon_V5_S04380110_Regions.interval_list VALIDATION_STRINGENCY=SILENT METRIC_ACCUMULATION_LEVEL=SAMPLE &


java -Xmx6g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T GenotypeGVCFs \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-D /mnt/Data1/resources/dbsnp_141.b37.vcf \
-A FisherStrand -A QualByDepth -A ChromosomeCounts -A VariantType \
-o WE323trio.HC-3.3-0.vcf \
-V /mnt/Data2/exome_sequencing/WE318_WE329/WE323.merge.HC-3.3-0.gVCF \
-V /mnt/Data2/exome_sequencing/WE259_WE283/WE282.merge.HC-3.3-0.gVCF \
-V /mnt/Data2/exome_sequencing/WE259_WE283/WE283.merge.HC-3.3-0.gVCF \
-log logs/GenotypeGVCF.WE323trio.log

java -Xmx6g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T VariantFiltration \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
--filterExpression "QD < 2.0" --filterName "QD2" \
--filterExpression "MQ < 40.0" --filterName "MQ40" \
--filterExpression "ReadPosRankSum < -8.0" --filterName "RPRS-8" \
--filterExpression "FS > 60.0" --filterName "FS60" \
--filterExpression "MQRankSum < -12.5" --filterName "MQRankSum-12.5" \
--variant WE323trio.HC-3.3-0.vcf \
-o WE323trio.HC-3.3-0.filtered.vcf \
-log logs/WE323trio.HC-3.3-0.filtered.log

java -Xmx4g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE323trio.HC-3.3-0.filtered.vcf \
--discordance /mnt/Data1/resources/common_no_known_medical_impact_20140929.vcf \
-o WE323trio.HC-3.3-0.filtered.no_common_dbSNP.vcf \
-log logs/SelectVariants.WE323trio.no_common_dbSNP.log

java -Xmx4g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE323trio.HC-3.3-0.filtered.no_common_dbSNP.vcf \
--discordance /mnt/Data2/exome_sequencing/HaplotypeCallerVariants/WE107-WE298.repeat.HC_3.1-1.variants_to_exclude.vcf \
-o WE323trio.HC-3.3-0.filtered.no_common_dbSNP_inhouse.vcf \
-log logs/SelectVariants.WE323trio.no_common_dbSNP_inhouse.log

java -Xmx4g -jar /usr/share/gatk/GenomeAnalysisTK-3.3-0/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE323trio.HC-3.3-0.filtered.no_common_dbSNP_inhouse.vcf \
--discordance /mnt/Data1/resources/ExAC.r0.1.sites.vep.AF5.vcf \
-o WE323trio.HC-3.3-0.filtered.no_common_dbSNP_inhouse_ExAC.vcf \
-log logs/SelectVariants.WE323trio.no_common_dbSNP_inhouse_ExAC.log

/mnt/Data3/alamut-batch-1.2.0/alamut-ht \
--hgmdUser monogenic_research@pms --hgmdPasswd HNF1A_DupC \
--in WE323trio.HC-3.3-0.filtered.no_common_dbSNP_inhouse_ExAC.vcf \
--ann WE323trio.HC-3.3-0.alamut.txt \
--unann WE323trio.HC-3.3-0.alamut.unannotated.txt \
--ssIntronicRange 2 \
--outputVCFInfo VariantType AC AF AN DP FS MQ QD \
--outputVCFGenotypeData GT AD DP GQ \
--outputVCFQuality \
--outputVCFFilter