java -Xmx24g -jar /usr/share/gatk/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T GenotypeGVCFs \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-D /mnt/Data1/resources/dbsnp_138.b37.vcf \
-A FisherStrand -A QualByDepth -A ChromosomeCounts -A VariantType \
-o WE310-WE311.HC_3.2-2.vcf \
-V WE330.HC_3.2-2.gVCF \
-V WE331.HC_3.2-2.gVCF \
-log logs/GenotypeGVCF.WE330-WE331.log

java -Xmx24g -jar /usr/share/gatk/GenomeAnalysisTK-3.1-1/GenomeAnalysisTK.jar -T VariantFiltration \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
--filterExpression "QD < 2.0" --filterName "QD2" \
--filterExpression "MQ < 40.0" --filterName "MQ40" \
--filterExpression "ReadPosRankSum < -8.0" --filterName "RPRS-8" \
--filterExpression "FS > 60.0" --filterName "FS60" \
--filterExpression "MQRankSum < -12.5" --filterName "MQRankSum-12.5" \
--variant WE310-WE311.HC_3.2-2.vcf \
-o WE310-WE311.HC_3.2-2.filtered.vcf \
-log logs/WE310-WE311.HC_3.2-2.filtered.log

java -Xmx24g -jar /usr/share/gatk/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE310-WE311.HC_3.2-2.filtered.vcf \
--discordance /mnt/Data1/resources/common_no_known_medical_impact_20140303.vcf \
-o WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP.vcf \
-log logs/SelectVariants.WE310-WE311.no_common_dbSNP.log

java -Xmx24g -jar /usr/share/gatk/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP.vcf \
--discordance /mnt/Data2/exome_sequencing/HaplotypeCallerVariants/WE107-WE298.repeat.HC_3.1-1.variants_to_exclude.vcf \
-o WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse.vcf \
-log logs/SelectVariants.WE310-WE311.no_common_dbSNP_inhouse.log

java -Xmx4g -jar /usr/share/gatk/GenomeAnalysisTK-3.2-2/GenomeAnalysisTK.jar -T SelectVariants \
-R /mnt/Data1/resources/human_g1k_v37.fasta \
-V WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse.vcf \
--discordance /mnt/Data1/resources/ExAC.r0.1.sites.vep.AF5.vcf \
-o WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse_ExAC.vcf \
-log logs/SelectVariants.WE310-WE311.no_common_dbSNP_inhouse.log

/mnt/Data3/alamut-batch-1.2.0/alamut-ht \
--hgmdUser monogenic_research@pms --hgmdPasswd HNF1A_DupC \
--in WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse_ExAC.vcf \
--ann WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse_ExAC.alamut.txt \
--unann WE310-WE311.HC_3.2-2.filtered.no_common_dbSNP_inhouse_ExAC.unannotated.txt \
--ssIntronicRange 2 \
--outputVCFInfo VariantType AC_Orig AF_Orig AN_Orig AC AF AN DP FS MQ QD \
--outputVCFGenotypeData GT AD DP GQ \
--outputVCFQuality \
--outputVCFFilter